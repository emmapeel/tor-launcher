<!ENTITY torsettings.dialog.title "Impostazioni della Rete Tor">
<!ENTITY torsettings.wizard.title.default "Connetti a Tor">
<!ENTITY torsettings.wizard.title.configure "Impostazioni della Rete Tor">
<!ENTITY torsettings.wizard.title.connecting "Connessione in corso">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Lingua per il Tor Browser">
<!ENTITY torlauncher.localePicker.prompt "Selezionare una lingua.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Clicca &quot;Connetti&quot; per connetterti a Tor">
<!ENTITY torSettings.configurePrompt "Clicca &quot;Configura&quot; per regolare le impostazioni di rete se ti trovi in un Paese che censura Tor (come Egitto, Cina, Turchia) o se ti stai connettendo da una rete privata che richiede un proxy.">
<!ENTITY torSettings.configure "Configura">
<!ENTITY torSettings.connect "Connetti">

<!-- Other: -->

<!ENTITY torsettings.startingTor "In attesa di avviare Tor...">
<!ENTITY torsettings.restartTor "Riavvia Tor">
<!ENTITY torsettings.reconfigTor "Riconfigura">

<!ENTITY torsettings.discardSettings.prompt "Tu hai configurato i bridge Tor o hai inserito le impostazioni per il proxy locale.&#160; Per effettuare una connessione diretta alla rete Tor, queste impostazioni devono essere rimosse.">
<!ENTITY torsettings.discardSettings.proceed "Rimuovi Impostazioni e Connetti">

<!ENTITY torsettings.optional "Facoltativo">

<!ENTITY torsettings.useProxy.checkbox "Uso un proxy per connettermi a internet">
<!ENTITY torsettings.useProxy.type "Tipo di proxy">
<!ENTITY torsettings.useProxy.type.placeholder "Seleziona un tipo di proxy">
<!ENTITY torsettings.useProxy.address "Indirizzo">
<!ENTITY torsettings.useProxy.address.placeholder "Indirizzo IP oppure hostname">
<!ENTITY torsettings.useProxy.port "Porta">
<!ENTITY torsettings.useProxy.username "Username">
<!ENTITY torsettings.useProxy.password "Password">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Questo computer passa attraverso un firewall che permette le connessioni solo ad alcune porte">
<!ENTITY torsettings.firewall.allowedPorts "Porte consentite">
<!ENTITY torsettings.useBridges.checkbox "Tor viene censurato nel mio Paese">
<!ENTITY torsettings.useBridges.default "Seleziona un bridge integrato">
<!ENTITY torsettings.useBridges.default.placeholder "seleziona un bridge">
<!ENTITY torsettings.useBridges.bridgeDB "Richiedi un bridge da torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Inserisci i caratteri dell'immagine">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Carica una nuova verifica">
<!ENTITY torsettings.useBridges.captchaSubmit "Invia">
<!ENTITY torsettings.useBridges.custom "Fornisci un bridge che conosco">
<!ENTITY torsettings.useBridges.label "Inserisci le informazioni del bridge da una fonte fidata.">
<!ENTITY torsettings.useBridges.placeholder "digita indirizzo:porta (uno per riga)">

<!ENTITY torsettings.copyLog "Copia il log di Tor negli &quot;appunti&quot; di sistema">

<!ENTITY torsettings.proxyHelpTitle "Aiuto proxy">
<!ENTITY torsettings.proxyHelp1 "Potrebbe essere necessario utilizzare un proxy locale quando ci si connette dalla rete di un'azienda, scuola o università.&#160;Se non sei sicuro sulla necessità di utilizzare un proxy, confronta le impostazioni internet in un altro browser o controlla le impostazioni di rete del sistema.">

<!ENTITY torsettings.bridgeHelpTitle "Aiuto per i ponti relé">
<!ENTITY torsettings.bridgeHelp1 "I bridge sono relay non elencati che rendono più difficile bloccare le connessioni alla rete Tor.&#160; Ogni tipo di bridge usa un metodo diverso per evitare la censura.&#160; Quelli obfs mascherano il tuo traffico come rumore casuale, quelli meek lo mascherano come se si connettesse a quel servizio al posto di Tor.">
<!ENTITY torsettings.bridgeHelp2 "A causa dei tentativi di blocco di Tor in alcuni Paesi, certi bridge funzionano in certi Paesi, ma non in altri.&#160; Se non sei sicuro di quali bridge funzionino nel tuo Paese, visita torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Attendi mentre stabiliamo una connessione alla rete Tor.&#160; Potrebbe richiedere qualche minuto.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Connessione">
<!ENTITY torPreferences.torSettings "Impostazioni di Tor">
<!ENTITY torPreferences.torSettingsDescription "Tor Browser indirizza il tuo traffico nella rete Tor, gestita da migliaia di volontari in tutto il mondo." >
<!ENTITY torPreferences.learnMore "Per saperne di più">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "Test">
<!ENTITY torPreferences.statusInternetOnline "Online">
<!ENTITY torPreferences.statusInternetOffline "Offline">
<!ENTITY torPreferences.statusTorLabel "Tor Network:">
<!ENTITY torPreferences.statusTorConnected "Connesso">
<!ENTITY torPreferences.statusTorNotConnected "Non Connesso">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "Per saperne di più">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Quickstart">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "Connetti sempre automaticamente">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Bridges">
<!ENTITY torPreferences.bridgesDescription "I bridge ti aiutano ad accedere alla rete Tor in luoghi dove Tor viene bloccato. A seconda di dove ti trovi, un bridge può funzionare meglio di un altro.">
<!ENTITY torPreferences.bridgeLocation "Your location">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automatica">
<!ENTITY torPreferences.bridgeLocationFrequent "Frequently selected locations">
<!ENTITY torPreferences.bridgeLocationOther "Other locations">
<!ENTITY torPreferences.bridgeChooseForMe "Choose a Bridge For Me…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Your Current Bridges">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can keep one or more bridges saved, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 bridge: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Rimuovi">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Disable built-in bridges">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Copy Bridge Address">
<!ENTITY torPreferences.copied "Copiato!">
<!ENTITY torPreferences.bridgeShowAll "Show All Bridges">
<!ENTITY torPreferences.bridgeRemoveAll "Remove All Bridges">
<!ENTITY torPreferences.bridgeAdd "Add a New Bridge">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choose from one of Tor Browser’s built-in bridges">
<!ENTITY torPreferences.bridgeSelectBuiltin "Select a Built-In Bridge…">
<!ENTITY torPreferences.bridgeRequest "Richiedi un bridge...">
<!ENTITY torPreferences.bridgeEnterKnown "Enter a bridge address you already know">
<!ENTITY torPreferences.bridgeAddManually "Add a Bridge Manually…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Avanzate">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Settings…">
<!ENTITY torPreferences.viewTorLogs "View the Tor logs">
<!ENTITY torPreferences.viewLogs "Visualizza Log...">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Remove all the bridges?">
<!ENTITY torPreferences.removeBridgesWarning "This action cannot be undone.">
<!ENTITY torPreferences.cancel "Annulla">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scan the QR code">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Built-In Bridges">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Richiedi Bridge">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Contattando BridgeDB. Attendere, prego.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Risolvi il CAPTCHA per richiedere un bridge.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "La soluzione è sbagliata. Riprova.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "Inserisci le informazioni del bridge da una fonte fidata">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Connection Settings">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Log di Tor">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Non Connesso">
<!ENTITY torConnect.connectingConcise "Connessione in corso...">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "Automatica">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Frequently selected locations">
<!ENTITY torConnect.otherLocations "Other locations">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "Configura connessione…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "Riprova">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Le modifiche alle impostazioni di Tor non verranno applicate finché non ti connetti">
<!ENTITY torConnect.tryAgainMessage "Tor Browser non è riuscito a stabilire una connessione alla rete Tor">
<!ENTITY torConnect.yourLocation "Your Location">
<!ENTITY torConnect.tryBridge "Prova un bridge">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
