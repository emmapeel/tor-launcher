<!ENTITY torsettings.dialog.title "إعدادات شبكة تور">
<!ENTITY torsettings.wizard.title.default "اتصل بتور">
<!ENTITY torsettings.wizard.title.configure "إعدادات شبكة تور">
<!ENTITY torsettings.wizard.title.connecting "يجري إنشاء اتصال">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "لغة متصفح تور">
<!ENTITY torlauncher.localePicker.prompt "الرجاء اختيار لغة.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "انقر &quot;اتصل&quot; للاتصال بتور.">
<!ENTITY torSettings.configurePrompt "انقر &quot;اضبط&quot; لضبط إعدادات الشبكة إن كنت في بلد يحجب تور (مثل مصر، والصين، وتركيا) أو إن كنت تتصل من شبكة خاصة تتطلب وسيط.">
<!ENTITY torSettings.configure "تكوين">
<!ENTITY torSettings.connect "اتصل">

<!-- Other: -->

<!ENTITY torsettings.startingTor "في انتظار تور حتي يبدء بالعمل...">
<!ENTITY torsettings.restartTor "أعِد تشغيل تور">
<!ENTITY torsettings.reconfigTor "أعِد ضبط">

<!ENTITY torsettings.discardSettings.prompt "ضبطت جسر تور أو أدخلت إعدادات الوسيط المحلي.&#160; لإنشاء اتصال مباشر بشبكه تور، يجب إزاله هذه الإعدادات.">
<!ENTITY torsettings.discardSettings.proceed "ازل الاعدادات ثم اتصل">

<!ENTITY torsettings.optional "اختياري">

<!ENTITY torsettings.useProxy.checkbox "أستخدم وسيطا للاتصال بالإنترنت">
<!ENTITY torsettings.useProxy.type "نوع البروكسي">
<!ENTITY torsettings.useProxy.type.placeholder "اختر نوع الوسيط">
<!ENTITY torsettings.useProxy.address "العنوان">
<!ENTITY torsettings.useProxy.address.placeholder "عنوان الإنترنت IP أو اسم المضيف">
<!ENTITY torsettings.useProxy.port "منفذ">
<!ENTITY torsettings.useProxy.username "اسم المستخدم">
<!ENTITY torsettings.useProxy.password "‮كلمة السّر">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "اتصال الإنترنت لهذا الكمبيوتر يمر بجدار حماية يسمح بالاتصال فقط من خلال منافذ معينة">
<!ENTITY torsettings.firewall.allowedPorts "المنافذ المسموح بها">
<!ENTITY torsettings.useBridges.checkbox "تور محجوب في بلدي">
<!ENTITY torsettings.useBridges.default "اختر جسرا مُدمَجا في البرنامج">
<!ENTITY torsettings.useBridges.default.placeholder "اختر جسرا">
<!ENTITY torsettings.useBridges.bridgeDB "اطلب جسرا من torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "أدخل الرموز الموجودة في الصورة">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "احصل على تحد جديد">
<!ENTITY torsettings.useBridges.captchaSubmit "أرسل">
<!ENTITY torsettings.useBridges.custom "أدخل عنوان جسرٍ أعرفه">
<!ENTITY torsettings.useBridges.label "أدخل معلومات جسر من مصدر موثوق.">
<!ENTITY torsettings.useBridges.placeholder "اكتب العنوان:المنفذ (واحد لكل سطر)">

<!ENTITY torsettings.copyLog "نسخ سجل تور إلي الحافظة">

<!ENTITY torsettings.proxyHelpTitle "مساعدة الوسيط">
<!ENTITY torsettings.proxyHelp1 "قد تكون هناك حاجة إلى وسيط (proxy) محلي عند الاتصال من خلال شركة أو مدرسة أو شبكة جامعية. إذا لم تكن متأكدًا مما إذا كان الوسيط مطلوبًا أم لا، فابحث عن إعدادات الإنترنت في متصفح آخر أو تحقق من إعدادات شبكة النظام لديك.">

<!ENTITY torsettings.bridgeHelpTitle "المساعدة الخاصة بالجسور المُرحلة">
<!ENTITY torsettings.bridgeHelp1 "الجسور هي تحويلات غير مدرجة تجعل حجب الاتصالات إلى شبكة تور أصعب.&#160; كل نوع من الجسور يستخدم طريقة مختلفة لتجنب الرقابة.&#160; جسور obfs تجعل حركة معلوماتك تبدو كضجيج عشوائي، وجسور meek تجعل حركة معلوماتك تبدوا كأنها تتصل لتلك الخدمة بدلا من تور.">
<!ENTITY torsettings.bridgeHelp2 "بسبب طريقة حظر بعض البلدان لتور، تعمل بعض الجسور في بلدان معينة لكنها لا تعمل ببعضها الآخر. إن لم تكن متأكدا بشأن أي الجسور تعمل في بلدك، فزر  torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "الرجاء الانتظار بينما ننشئ اتصالا لشبكة تور.&#160; قد يستغرق هذا عدة دقائق.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "الاتصال">
<!ENTITY torPreferences.torSettings "إعدادات تور Tor">
<!ENTITY torPreferences.torSettingsDescription "متصفح Tor Browser يوجه حركة بياناتك عبر شبكة تور Tor Network, التي يشغلها آلاف المتطوعون حول العالم." >
<!ENTITY torPreferences.learnMore "تعرف على المزيد">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "افحص">
<!ENTITY torPreferences.statusInternetOnline "متصل">
<!ENTITY torPreferences.statusInternetOffline "غير متصل">
<!ENTITY torPreferences.statusTorLabel "Tor Network:">
<!ENTITY torPreferences.statusTorConnected "متصل">
<!ENTITY torPreferences.statusTorNotConnected "غير متصل">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "تعرف على المزيد">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "بداية سريعة">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "اتصل تلقائيا بشكل دائم">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "الجسور">
<!ENTITY torPreferences.bridgesDescription "تساعدك الجسور على دخول شبكة تور Tor Network في الأماكن التي تحظر تور Tor. وبالاعتماد على مكان تواجدك فإن أحد الجسور قد يعمل بشكل أفضل من غيره.  ">
<!ENTITY torPreferences.bridgeLocation "Your location">
<!ENTITY torPreferences.bridgeLocationAutomatic "تلقائي">
<!ENTITY torPreferences.bridgeLocationFrequent "Frequently selected locations">
<!ENTITY torPreferences.bridgeLocationOther "Other locations">
<!ENTITY torPreferences.bridgeChooseForMe "Choose a Bridge For Me…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Your Current Bridges">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can keep one or more bridges saved, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 bridge: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "حذف">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Disable built-in bridges">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Copy Bridge Address">
<!ENTITY torPreferences.copied "تم النسخ!">
<!ENTITY torPreferences.bridgeShowAll "Show All Bridges">
<!ENTITY torPreferences.bridgeRemoveAll "Remove All Bridges">
<!ENTITY torPreferences.bridgeAdd "Add a New Bridge">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choose from one of Tor Browser’s built-in bridges">
<!ENTITY torPreferences.bridgeSelectBuiltin "Select a Built-In Bridge…">
<!ENTITY torPreferences.bridgeRequest "اطلب جسرا…">
<!ENTITY torPreferences.bridgeEnterKnown "Enter a bridge address you already know">
<!ENTITY torPreferences.bridgeAddManually "Add a Bridge Manually…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "متقدم">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Settings…">
<!ENTITY torPreferences.viewTorLogs "View the Tor logs">
<!ENTITY torPreferences.viewLogs "شاهد السجلات...">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Remove all the bridges?">
<!ENTITY torPreferences.removeBridgesWarning "This action cannot be undone.">
<!ENTITY torPreferences.cancel "إلغاء ">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scan the QR code">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Built-In Bridges">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "اطلب جسراً">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "يتصل بقاعدة بيانات الجسور BridgeDB. يرجى الانتظار.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "حل كاباتشا لطلب جسر.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "الحل ليس سليمًا. يُرجى إعادة المحاولة.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "أدخل معلومات الجسر من مصدر موثوق">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Connection Settings">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "سجلات تور Tor">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "غير متصل">
<!ENTITY torConnect.connectingConcise "جاري الاتصال ...">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "تلقائي">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Frequently selected locations">
<!ENTITY torConnect.otherLocations "Other locations">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "تكوين الاتصال…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "الرجاء المحاولة مجددًا">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "لن تسري التغييرات على إعدادات Tor حتى تقوم بالاتصال">
<!ENTITY torConnect.tryAgainMessage "فشل متصفح Tor في إنشاء اتصال بشبكة Tor">
<!ENTITY torConnect.yourLocation "Your Location">
<!ENTITY torConnect.tryBridge "جرب الجسر">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
